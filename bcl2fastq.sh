#!/bin/sh

#!/bin/bash
#SBATCH --job-name=bcl2fastq
#SBATCH -p serial
#SBATCH --time=00:30:00
#SBATCH -o job.%J.out
#SBATCH -e job.%J.err

# Illumina Experiment Manager (IEM) sample sheet format used by bcl2fastq
# https://www.well.ox.ac.uk/ogc/wp-content/uploads/2017/09/Bcl2FastQ2_Stand_Alone_external_B.pdf

# samplesheet details, for 10X genomics
# http://research.fhcrc.org/content/dam/stripe/sun/software/scRNAseq/scRNAseq.html#211_generate_fastq_files

#submit job 
#sbatch --cpus-per-task=12 bcl2fastq_job2.sh

#squeue -u $USER
#scontrol show job 384
#scancel 384
#sinfo

#Load your modules
module load bcl2fastq/2.20.0.422

# main project directory
PROJ_DIR=$HOME/JPE21292/190801_NB551577_0019_AHV5HCBGXB

# create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/FASTQ
if [ ! -d ${OUT_DIR} ]; then
 mkdir -p ${OUT_DIR}
fi

#  -r [ --loading-threads ] arg (=4)               number of threads used for loading BCL data
#  -p [ --processing-threads ] arg                 number of threads used for processing demultiplexed data
#  -w [ --writing-threads ] arg (=4)               number of threads used for writing FASTQ data.

# -i [ --input-dir ] arg (=<runfolder-dir>/Data/Intensities/BaseCalls/)
#                                                  path to input directory
# -R [ --runfolder-dir ] arg (=./)                 path to runfolder directory
# --intensities-dir arg (=<input-dir>/../)         path to intensities directory
#                                                  If intensities directory is specified, --input-dir must also be specified.
#  -o [ --output-dir ] arg (=<input-dir>)          path to demultiplexed output

# --interop-dir arg (=<runfolder-dir>/InterOp/)   path to demultiplexing statistics directory
# --stats-dir arg (=<output-dir>/Stats/)          path to human-readable demultiplexing statistics directory
# --reports-dir arg (=<output-dir>/Reports/)      path to reporting directory
# --sample-sheet arg (=<runfolder-dir>/SampleSheet.csv)

# -R $PROJ_DIR \
# --intensities-dir $PROJ_DIR/Data/Intensities \
# --interop-dir $PROJ_DIR/InterOp \
# --stats-dir $OUT_DIR/Stats \
# --reports-dir $OUT_DIR/Reports \

# --no-lane-splitting 

bcl2fastq -r 10 -p 10 -w 4 \
 --use-bases-mask Y100,I8,Y10,Y100 \
 --input-dir $PROJ_DIR/Data/Intensities/BaseCalls/ \
 --runfolder-dir $PROJ_DIR \
 --intensities-dir $PROJ_DIR/Data/Intensities/ \
 --sample-sheet $PROJ_DIR/SampleSheet.csv \
 --barcode-mismatches 1 \
 --mask-short-adapter-reads 0 \
 --create-fastq-for-index-reads \
 -o $OUT_DIR

module unload bcl2fastq/2.20.0.422
